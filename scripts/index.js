function show_hide_trigger() {
	let block = document.getElementById("navbar-menu");
	if (block.style.display != "block") {
		block.style.display = "block";
		block.style.background = "rgba(0, 0, 0, 70%)";
	}
	else {
		block.style.display = "none";
	}
}
